from pymongo import MongoClient
import json
import re
from env import *
import random
import requests
import re
import logging
logging.getLogger('').handlers = []
logging.basicConfig(format='%(asctime)s.%(msecs)03d %(levelname)s %(message)s',level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')

def fetch_faq_response_api(group_id,intent_name,entity_name,entity_value,platform,sender_id):
    headers = {
        "Content-Type": "application/json",
        "authorization": FAQ_AUTHORIZATION
    }
    if entity_name is not None:
        body = { 
            "data": {
                "_action": "intentEntityResponse", 
                "_param": { 
                    "group_id": group_id, 
                    "intent_name": intent_name, 
                    "entity_name": entity_name, 
                    "entity_value": entity_value, 
                    "language": "en",
                    "platform": platform,
                    "session_id" : sender_id
                } 
            } 
        }
    else:
        body = { 
            "data": {
                "_action": "intentEntityResponse", 
                "_param": { 
                    "group_id": group_id,
                    "intent_name": intent_name, 
                    "language": "en",
                    "platform": platform,
                    "session_id" : sender_id
                } 
            } 
        }
    try:
        logging.info("session_id:" + sender_id + "~rasa_cms_request~" + json.dumps(body))
        response = requests.post(url=FAQ_API_URI, data=json.dumps(body), headers=headers)
        data = response.json()
        logging.info("session_id:" + sender_id + "~rasa_cms_response~" + json.dumps(data))
        return data
    except:
        return None



def format_api_response(response):
    logging.info("I am in format api response : ")
    logging.info(response)
    if response["success"] == 1:
        logging.info(response["data"])
        response = response["data"]
        not_null = []
        for i in response["response_details"]:
            if "text_input" in i:
                not_null.append(i)

        response["response_details"] = not_null
        logging.info("logging line no 68")
        #Get a randomized message
        random_string = random.choice(response["response_details"])

        #Extract the random message from the object
        logging.info("logging line no 73")
        random_message = random_string['text_input']
        
        #Extract the random ssml from the object
        logging.info("logging line no 77")
        random_ssml = random_string['ssml_input']
        
        #Replace the title array with random title message
        logging.info("logging line no 81")
        response['response_details'] = random_message
        try:
            authentication_required = response["authentication_required"]
            logging.info("logging line no 85")
        except:
            logging.info("logging line no 87")
            authentication_required = "N"
            
        if "action_param_use_case_code" in response:
            logging.info("logging line no 91")
            action_param_use_case_code = response["action_param_use_case_code"]
        elif "use_case_code" in response:
            logging.info("logging line no 94")
            action_param_use_case_code = response["use_case_code"]
        else:
            logging.info("logging line no 97")
            action_param_use_case_code = ""

        if "post_action" in response:
            logging.info("logging line no 101")
            post_action = response["post_action"]
        else:
            logging.info("logging line no 104")
            post_action = None
        
        if "post_data" in response:
            logging.info("logging line no 107")
            post_data = response["post_data"]
        else:
            logging.info("logging line no 111")
            post_data = None

        if authentication_required == "N":
            logging.info("logging line no 115")
            authentication_required = False
        else:
            logging.info("logging line no 118")
            authentication_required = True


        logging.info("logging line no 122")
        message = [{"message":{"template":{"elements":{}, "type": "Card"}}},{"ssml":""}]
        buttons = False
        for key,value in response.items():
            if key == "response_details":
                message[0]["message"]["template"]["elements"]["title"] = value
                logging.info("logging line no 128")
                message[0]["message"]["template"]["elements"]["says"] = ""
                message[0]["message"]["template"]["elements"]["visemes"] = ""
                message[0]["message"]["template"]["type"] = "Card"
                logging.info("logging line no 132")
                random_ssml = random_ssml.replace("*", "")
                message[1]["ssml"] = random_ssml
                logging.info("logging line no 135")
                message[0]["payload"] = {"authentication_required": authentication_required,"action_param_use_case_code":action_param_use_case_code, "post_action": post_action, "post_data": post_data}
            elif key == "buttons" and len(value) > 0:
                buttons = True
                value[0]["type"] = "postback"
                logging.info("logging line no 140")
                for i in value:
                    i["icon"] = ""
                value = sorted(value, key= lambda i: i["seq"])
                message[0]["message"]["template"]["elements"]["buttons"] = value
                logging.info("logging line no 145")
                message[0]["message"]["template"]["type"] = "Card"
            
        if buttons == True:
            logging.info("logging line no 149")
            message[0]["message"]["template"]["type"] = "Card"
    

    elif response["success"] == 0 or response["success"] == 2 or response["success"] == 3:
        logging.info("logging line no 154")
        message = [{"message":{"template":{"elements":{}, "type": "Card"}}},{"ssml":""}]
        message[0]["message"]["template"]["elements"]["title"] = ""

        # message[1]["ssml"] = "<speak><s>Can you please say that again?</s></speak>" 

    else:
        # errors = response["errors"]
        logging.info("logging line no 163")
        message = [{"message":{"template":{"elements":{}, "type": "Card"}}},{"ssml":""}]

        message[0]["message"]["template"]["elements"]["title"] = "Can you please say that again?"

        message[1]["ssml"] = "Can you please say that again?"
    
    # finalResponse = {
    #     "success": 1,
    #     "message": message
    # }
    return message
